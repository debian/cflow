Format: https://www.debian.org/doc/packaging-manuals/copyright-format/1.0/
Upstream-Name: cflow
Upstream-Contact: bug-cflow@gnu.org
Source: http://www.gnu.org/software/cflow

Files: *
Copyright: 1994-2021 Sergey Poznyakoff <gray@gnu.org>
License: GPL-3+

Files: doc/cflow.info
       doc/cflow.texi
Copyright: 2005-2021 Sergey Poznyakoff <gray@gnu.org>
License: GFDL-1.2+

Files: doc/gendocs.sh
       gnu/*
       po/*
       tests/*
Copyright: 1987-2021 Free Software Foundation, Inc.
           2005-2021 Sergey Poznyakoff <gray@gnu.org>
           2006-2010 Jerry St.Clair <jds.2005@verizon.net>
License: GPL-3+

Files: doc/imprimatur/README
       NEWS
       README
       TODO
Copyright: 2005-2021 Sergey Poznyakoff <gray@gnu.org>
License: permissive-short-disclaimer

Files: debian/*
Copyright: 1996-1997 Dominik Kubla <dominik@debian.org>
           1997      Ioannis Tambouras <ioannis@debian.org>
           1998      Anthony Towns <ajt@debian.org>
           2001-2003 Richard Braakman <dark@xs4all.nl>
           2004      Sebastian Muszynski <do2ksm@linkt.de>
           2006-2009 Bart Martens <bartm@debian.org>
           2009-2010 Jakub Wilk <jwilk@debian.org>
           2010-2014 Serafeim Zanikolas <sez@debian.org>
           2018-2021 Dmitry Bogatov <KAction@debian.org>
           2021-2023 Marcos Talau <talau@debian.org>
License: GPL-2+

License: GFDL-1.2+
 Permission is granted to copy, distribute and/or modify this document
 under the terms of the GNU Free Documentation License, Version 1.2
 or any later version published by the Free Software Foundation;
 with no Invariant Sections, no Front-Cover Texts, and no Back-Cover
 Texts.
 .
 On Debian GNU/Linux systems, the complete text of the GNU Free
 Documentation License can be found in
 `/usr/share/common-licenses/GFDL-1.2'.

License: GPL-2+
 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; either version 2 of the License, or
 (at your option) any later version.
 .
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 .
 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
 .
 On Debian systems the full text of the GNU General Public License
 Version 2 can be found in the /usr/share/common-licenses/GPL-2 file.

License: GPL-3+
 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; either version 3 of the License, or
 (at your option) any later version.
 .
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 .
 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
 .
 On Debian systems the full text of the GNU General Public License
 Version 3 can be found in the /usr/share/common-licenses/GPL-3 file.

License: permissive-short-disclaimer
 Permission is granted to anyone to make or distribute verbatim
 copies of this document as received, in any medium, provided
 that the copyright notice and this permission notice are
 preserved, thus giving the recipient permission to redistribute
 in turn.
 .
 Permission is granted to distribute modified versions of this
 document, or of portions of it, under the above conditions,
 provided also that they carry prominent notices stating who
 last changed them.
