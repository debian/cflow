#include <stdio.h>

void fun1(void) {
}

void fun2(void) {
}

void fun3(void) {
        fun4();
}

void fun4(void) {
        fun5();
}

void fun5(void) {
}

void fun_outof_main() {
        fun5();
}

int main(int argc, char **argv) {
        printf("starting...\n");

        fun1();
        fun2();
        fun3();

        return 0;
}
